
Drupal.webform_ajaxifier = {
	"submit": function() {
		var form = $(this), content = form.parent(), block = content.parent();
		jQuery.post(form.attr('action'), form.serialize(), function(rsp) {
			if ( 0 === rsp.indexOf('OK') ) {
				// success: replace entire block with success message
				block.html(rsp.substr(2));
			}
			else {
				// FAIL!: replace block content with new form (incl <form>)
				content.html(rsp);
				$('form', content).submit(Drupal.webform_ajaxifier.submit);
			}
		});
		return false;
	}
};

$(function() {
	if ( Drupal.settings.webform_ajaxifier && Drupal.settings.webform_ajaxifier.webforms && 0 < Drupal.settings.webform_ajaxifier.webforms.length ) {
		$(Drupal.settings.webform_ajaxifier.webforms.join(', ')).submit(Drupal.webform_ajaxifier.submit);
	}
});
